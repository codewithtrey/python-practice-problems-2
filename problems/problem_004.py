# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    # if two values are the same maximum value, return either
    if value1 >= value2:
        return value1
    elif value2 >= value3:
        return value2
    else:
        return value3
    # if all three are the same maximum value, reutrh any
    # use the >= operator to compare using greater than or equal to

one = 6
two = 7
three = 8

print(max_of_three(one, two, three))
